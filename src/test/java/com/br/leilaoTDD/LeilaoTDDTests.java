package com.br.leilaoTDD;

import com.br.leilaoTDD.Models.Lance;
import com.br.leilaoTDD.Models.Leilao;
import com.br.leilaoTDD.Models.Leiloeiro;
import com.br.leilaoTDD.Models.Usuario;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class LeilaoTDDTests {
    @Test
    public void testarValidarLanceNOK() throws Exception {
        List<Lance> lances = new ArrayList<Lance>();
        lances.add(new Lance(new Usuario("Vinicius", 1), 30.0));
        lances.add(new Lance(new Usuario("Pedro", 2), 35.0));
        Usuario usuario = new Usuario("Rodrigo", 3);
        Lance lance = new Lance(usuario,10.0);
        Leilao leilao = new Leilao(lances);
        Assertions.assertEquals(leilao.validarLance(lance),false);
    }
    @Test
    public void testarValidarLanceOK() throws Exception {
        List<Lance> lances = new ArrayList<Lance>();
        lances.add(new Lance(new Usuario("Vinicius", 1), 30.0));
        lances.add(new Lance(new Usuario("Pedro", 2), 35.0));
        Usuario usuario = new Usuario("Rodrigo", 3);
        Lance lance = new Lance(usuario,40.0);
        Leilao leilao = new Leilao(lances);
        Assertions.assertEquals(leilao.validarLance(lance),true);
    }

    @Test
    public void testarAdicionarNovoLanceOK() throws Exception {
        List<Lance> lances = new ArrayList<Lance>();
        lances.add(new Lance(new Usuario("Vinicius", 1), 30.0));
        lances.add(new Lance(new Usuario("Pedro", 2), 35.0));
        Usuario usuario = new Usuario("Rodrigo", 3);
        Lance lance = new Lance(usuario,40.0);
        Leilao leilao = new Leilao(lances);
        leilao.adicionarNovoLance(lance);
        Assertions.assertTrue(leilao.getLances().contains(lance));
    }
    @Test
    public void testarAdicionarNovoLanceNOK() throws Exception {
        List<Lance> lances = new ArrayList<Lance>();
        lances.add(new Lance(new Usuario("Vinicius", 1), 30.0));
        lances.add(new Lance(new Usuario("Pedro", 2), 35.0));
        Usuario usuario = new Usuario("Rodrigo", 3);
        Lance lance = new Lance(usuario,10.0);
        Leilao leilao = new Leilao(lances);
        leilao.adicionarNovoLance(lance);
        Assertions.assertFalse(leilao.getLances().contains(lance));
    }
    @Test
    public void testarRetornarMaiorLanceOK() throws Exception {
        List<Lance> lances = new ArrayList<Lance>();
        lances.add(new Lance(new Usuario("Vinicius", 1), 30.0));
        lances.add(new Lance(new Usuario("Pedro", 2), 35.0));
        Usuario usuario = new Usuario("Rodrigo", 3);
        Lance lance = new Lance(usuario,10.0);
        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Tulio", leilao);
        Assertions.assertEquals(leiloeiro.retornarMaiorLance(leilao).getValorDoLance(), 35);
    }
    @Test
    public void testarRetornarUsuarioMaiorLanceOK() throws Exception {
        List<Lance> lances = new ArrayList<Lance>();
        lances.add(new Lance(new Usuario("Vinicius", 1), 30.0));
        lances.add(new Lance(new Usuario("Pedro", 2), 35.0));
        Usuario usuario = new Usuario("Rodrigo", 3);
        Lance lance = new Lance(usuario,10.0);
        Leilao leilao = new Leilao(lances);
        Leiloeiro leiloeiro = new Leiloeiro("Tulio", leilao);
        Assertions.assertEquals(leiloeiro.retornarUsuarioMaiorLance(leilao),"Pedro");
    }

}
