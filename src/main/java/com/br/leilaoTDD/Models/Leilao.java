package com.br.leilaoTDD.Models;

import java.util.List;

public class Leilao {
    private List<Lance> lances;

    public Leilao() {
    }

    public Leilao(List<Lance> lances) {
        this.lances = lances;
    }

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }
    public Lance adicionarNovoLance(Lance lance){
        Leilao leilao = new Leilao(lances);
        if(leilao.validarLance(lance) == true){
            lances.add(lance);
        }
        return lance;
    }
    public boolean validarLance(Lance lance){
        List<Lance> listaLeilao = getLances();
        for (Lance lancefor:lances) {
            if (lancefor.getValorDoLance() > lance.getValorDoLance()){
                return false;
            }
        }
        return true;
    }
    public Lance buscarMaiorValor(Leilao leilao) {
        List<Lance> listaLances = leilao.getLances();
        int indMaiorValor = 0;
        for (int i = 0; i < listaLances.size(); i++) {
            if (listaLances.get(i).getValorDoLance() >= listaLances.get(indMaiorValor).getValorDoLance()) {
                indMaiorValor = i;
            }
        }
        return listaLances.get(indMaiorValor);
    }
}
