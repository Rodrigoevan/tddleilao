package com.br.leilaoTDD.Models;

public class Usuario {
    private String nome;
    private Integer id;

    public Usuario() {
    }

    public String getNome() {
        return nome;
    }

    public Usuario(String nome, Integer id) {
        this.nome = nome;
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
