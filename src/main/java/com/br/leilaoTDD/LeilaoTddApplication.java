package com.br.leilaoTDD;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LeilaoTddApplication {

	public static void main(String[] args) {
		SpringApplication.run(LeilaoTddApplication.class, args);
	}

}
