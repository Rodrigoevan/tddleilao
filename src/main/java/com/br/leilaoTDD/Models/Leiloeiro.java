package com.br.leilaoTDD.Models;

public class Leiloeiro {
    private String nome;
    private Leilao leilao;

    public Leiloeiro() {
    }

    public Leiloeiro(String nome, Leilao leilao) {
        this.nome = nome;
        this.leilao = leilao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Leilao getLeilao() {
        return leilao;
    }

    public void setLeilao(Leilao leilao) {
        this.leilao = leilao;
    }

    public Lance retornarMaiorLance(Leilao leilao){
        Lance lanceObjeto = leilao.buscarMaiorValor(leilao);
        return lanceObjeto;
    }
    public String retornarUsuarioMaiorLance(Leilao leilao){
        Lance lanceObjeto = leilao.buscarMaiorValor(leilao);
        return lanceObjeto.getUsuario().getNome();
    }
}
